package com.backend.projeto.repositoy;

import com.backend.projeto.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User,  Long> {

    User findByEmail(String username);
}
